// Configure the app settings here such as Web port, certificates, etc.



// read from the file system (used for SSL certs)
var fs = require('fs');

module.exports = {
  port: 8080,
  key  : fs.readFileSync('./ssl/server.key'),
  cert : fs.readFileSync('./ssl/server.crt'),
  ca: [fs.readFileSync('./ssl/server.crt')]
};
